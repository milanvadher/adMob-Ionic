import { Component } from '@angular/core';
import { AdMobFree, AdMobFreeBanner, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig, AdMobFreeRewardVideoConfig } from "@ionic-native/admob-free";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(private admobFree: AdMobFree) {
    this.showBannerAd();
    this.showInterstitialAd();
    this.showVideoRewardsAd();
  }

  async showBannerAd() {
    try {
      const bannerConfig: AdMobFreeBannerConfig = {
        id: 'your_id',
        // isTesting: true,
        autoShow: true
      }

      this.admobFree.banner.config(bannerConfig);

      const result = this.admobFree.banner.prepare();
      console.log('showBannerAd: ', result);
    }
    catch (e) {
      console.log('showBannerAd: ', e);
    }

  }

  async showInterstitialAd() {
    try {
      const interstitialConfig: AdMobFreeInterstitialConfig = {
        id: 'your_id',
        // isTesting: true,
        autoShow: true
      }

      this.admobFree.interstitial.config(interstitialConfig);

      const result = this.admobFree.interstitial.prepare();
      console.log('showInterstitialAd: ', result);
    }
    catch (e) {
      console.log('showInterstitialAd: ', e)
    }

  }

  async showVideoRewardsAd() {
    try {
      const videoRewardsConfig: AdMobFreeRewardVideoConfig = {
        id: 'your_id',
        // isTesting: true,
        autoShow: true
      }

      this.admobFree.rewardVideo.config(videoRewardsConfig);

      const result = this.admobFree.rewardVideo.prepare();
      console.log('showVideoRewardsAd: ', result);
    }
    catch (e) {
      console.log('showVideoRewardsAd: ', e)
    }
  }

}
